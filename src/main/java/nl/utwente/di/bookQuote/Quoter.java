package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    double getBookPrice(String isbn) {
        HashMap<String, Double> bookPrices = new HashMap<>();
        bookPrices.put("1", 10.0);
        bookPrices.put("2", 45.0);
        bookPrices.put("3", 20.0);
        bookPrices.put("4", 35.0);
        bookPrices.put("5", 50.0);

        if (!bookPrices.containsKey(isbn)) {
            return 0.0;
        }
        return bookPrices.get(isbn);
    }
}
